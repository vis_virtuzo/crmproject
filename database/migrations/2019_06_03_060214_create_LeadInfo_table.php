<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LeadInfo', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('CompanyName');
            $table->string('ContactPerson');
            $table->string('ContactNumber');
            $table->string('ContactEmailId')->unique();
            $table->string('Address');
            $table->string('City');
            $table->string('State');
            $table->string('PinCode');
            $table->string('WebsiteURL');
            $table->string('RecordCreatedBy');
            $table->string('AssignedBD');
            $table->string('EventOnline');
            $table->string('DiscussionSummery');
            $table->string('CustomerTurnOver');
            $table->string('CustomerItSpend');
            $table->string('CustomerItPen');
            $table->string('LeadSource');
            $table->string('LeadStatus');
            $table->string('NoOfEmployee');
            $table->string('Industry');            
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LeadInfo');
    }
}
