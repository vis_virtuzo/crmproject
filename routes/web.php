<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('dashboard', function () {
    return view('index');
});
Route::get('crm', function ()
{
    return view('crm');
});
// Route::get('form', function(){
//     return view('form');
// });
Route::get('table', function(){
    return view('tables');
});
// Route::get('addevents', function(){
//     return view('addevents');
// });
Route::get('register',function()
{
    return view('register');
});

Route::get('getleads','GetLeadsController@GetLead');

Route::post('SaveLeads','SaveLeadsController@store');

// Route::get('GetLeadStatus','GetDropdownDataController@AllList');
Route::get('addleads','GetDropdownDataController@AllList');

Route::get('addevents','GetDropdownDataController@EventsAllList');

Route::post('SaveEvents','SaveEventsController@store');

Route::get('EditLeads/{id}','GetLeadsController@GetSingleLead');

Route::get('addevents/{id}','GetDropdownDataController@EventsAllList');

Route::POST('updateevent','SaveEventsController@update');

Route::get('deletelead/{id}','DeleteLeadsController@delete');

Route::get('deleteevents/{id}','DeleteEventsController@delete');

Route::get('editlead', function()
{
    return view('editlead');
});

Route::post('RegisterUser', 'SaveUsersController@store');

Route::post('auth','LoginChkController@Login');

Route::get('logout','LogoutController@logout');

// Route::get('login', 'LoginController@login');
Route::get('login', function()
{
    return view('login');
});

Route::POST('recover','PassRecoverController@recover');