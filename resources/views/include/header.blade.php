<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>
textarea {
	resize: none;
}
.form-label {
	font-size: 12px;
	color: #5e9bfc;
	margin: 0;
	display: block;
	opacity: 1;
	-webkit-transition: .333s ease top, .333s ease opacity;
	transition: .333s ease top, .333s ease opacity;
}
.form-control {
	border-radius: 0;
	border-color: #ccc;
   	border-width: 0 0 2px 0;
   	border-style: none none solid none;
   	box-shadow: none;
}
.form-control:focus {
	box-shadow: none;
	border-color: #5e9bfc;
}
.js-hide-label {
	opacity: 0; 	
}
.js-unhighlight-label {
	color: #999 
}
.btn-start-order {
	background: 0 0 #ffffff;
    border: 1px solid #2f323a;
    border-radius: 3px;
    color: #2f323a;
    font-family: "Raleway", sans-serif;
    font-size: 16px;
    line-height: inherit;
    margin: 30px 0;
    padding: 10px 50px;
    text-transform: uppercase;
    transition: all 0.25s ease 0s;
}
.btn-start-order:hover,.btn-start-order:active, .btn-start-order:focus {
	border-color: #5e9bfc;
	color: #5e9bfc;
}

/* .form-control {
    display: block;
    width: 80%;  
} */
</style>

<script>
$(document).ready(function() 
{
	// Test for placeholder support
    $.support.placeholder = (function()
    {
        var i = document.createElement('input');
        return 'placeholder' in i;
    })();

    // Hide labels by default if placeholders are supported
    if($.support.placeholder) 
    {
        $('.form-label').each(function()
        {
            $(this).addClass('js-hide-label');
        });  

        // Code for adding/removing classes here
        $('.form-group').find('input, textarea').on('keyup blur focus', function(e)
        {
            // Cache our selectors
            var $this = $(this),
            $label = $this.parent().find("label");		
            switch(e.type) 
            {
                case 'keyup': 
                {
                    $label.toggleClass('js-hide-label', $this.val() == '');
                } break;
                case 'blur': 
                {
                    if( $this.val() == '' ) 
                    {
                        $label.addClass('js-hide-label');
                    } 
                    else 
                    {
                        $label.removeClass('js-hide-label').addClass('js-unhighlight-label');
                    }
                } break;
                case 'focus': 
                {
                    if( $this.val() !== '' ) 
                    {
                        $label.removeClass('js-unhighlight-label');
                    }
                } break;
                default: break;
            }
			
        });
    } 
});
</script>
@yield('content')