@include('include.connect')
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Matrix Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/extra-libs/multicheck/multicheck.css">
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
.dataTables_filter {
    display: none;
}
th, td {
    white-space: nowrap;
}

#example{
    border-bottom: none;
}
#example tr:first-child td:not(.options){
    border-bottom: 1px solid;
}
#example .options{
    background: white;
    border: none;
}
table.dataTable {
    clear: both;
    margin-top: 6px !important;
    margin-bottom: 6px !important;
    max-width: none !important;
    border-collapse: collapse !important;
    width:100% !important;
}
.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}

/* **********************  Sweet alert popup  ********************** */
.swal-modal {
    width: 400px;
    opacity: 0;
    pointer-events: none;
    background-color: #fff;
    text-align: center;
    border-radius: 5px;
    position: static;
    margin: 20px auto;
    display: inline-block;
    vertical-align: middle;
    -webkit-transform: scale(1);
    transform: scale(1);
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    z-index: 10001;
    transition: opacity .2s,-webkit-transform .3s;
    transition: transform .3s,opacity .2s;
    transition: transform .3s,opacity .2s,-webkit-transform .3s;
}
/* ********************** End sweet alert popup ********************* */
</style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

    @include('include.sidebar')
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Leads</h4>
                        <!-- <a href="{{url('form')}}"><button class="btn btn-primary btn-sm" style="margin-left:10px">Add Lead</button></a> -->
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session()->get('message') }}</strong> 
                    </div>
                @endif
                @if(session()->has('Lead'))
                    <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session()->get('Lead') }}</strong> 
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered display">
                                        <thead>
                                            <tr>
                                                <th>

                                                <div class="dropdown" >
                                            <button class="btn btn-default dropdown-toggle" type="button" 
                                                    id="dropdownMenu1" data-toggle="dropdown" 
                                                    aria-haspopup="true" aria-expanded="true" >
                                                <i class="glyphicon glyphicon-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMenu1" style="overflow-y: scroll;height: 300px;">
                                            
                                                <li >
                                                <label>
                                                    <input id="LeadNo" value="1" type="checkbox"> Lead No
                                                </label>
                                                </li>
                                                
                                                <!-- <li >
                                                <label>
                                                    <input id="LeadOwner" value="2" type="checkbox"> Lead Owner
                                                </label>
                                                </li> -->
                                                
                                                <li >
                                                <label>
                                                    <input id="CompanyName" value="3" type="checkbox" > Company Name
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="Contactperson" value="4" type="checkbox" > Contact person
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="ContactNumber" value="5" type="checkbox" > Contact Number	
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="ContactEmailId" value="6" type="checkbox"  > Contact EmailId
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="Address" value="7" type="checkbox" checked> Address
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="City" value="8" type="checkbox" checked> City
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="State" value="9" type="checkbox" checked> State
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="PinCode" value="10" type="checkbox" checked> Pin Code
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="WebsiteURL" value="11" type="checkbox" checked> Website URL
                                                </label>
                                                </li>
                                                <!-- <li >
                                                <label>
                                                    <input id="RecordCreatedBy" value="12" type="checkbox" > Record CreatedBy
                                                </label>
                                                </li> -->
                                                <li >
                                                <label>
                                                    <input id="Assigned_BD" value="13" type="checkbox" > Assigned_BD
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="EventOnline" value="14" type="checkbox" checked> Event Date
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="CustomerTurnover" value="15" type="checkbox" checked> Customer Turnover
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="CustomerItSpend" value="16" type="checkbox" checked> Customer It Spend
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="LeadSource" value="17" type="checkbox" checked> Lead Source
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="LeadStatus" value="18" type="checkbox" checked> Lead Status
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="NoOfEmployees" value="19" type="checkbox" checked> NoOfEmployees
                                                </label>
                                                </li>
                                                <li >
                                                <label>
                                                    <input id="Industry" value="20" type="checkbox" checked> Industry
                                                </label>
                                                </li>
                                                
                                            </ul>
                                            </div>

                                                </th>
                                                <th>LeadNo</th>
                                                <th style="display:none;">Lead Owner</th>
                                                <th>Company Name</th>
                                                <th>Contact person</th>
                                                <th>Contact Number</th>
                                                <th>Contact EmailId</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>PinCode</th>
                                                <th>Website URL</th>
                                                <th style="display:none;">Record CreatedBy</th>
                                                <th>Assigned_BD</th>
                                                <th>Event Date</th>
                                                <th>Customer Turnover</th>
                                                <th>Customer It Spend</th>
                                                <th>Lead Source</th>
                                                <th>Lead Status</th>
                                                <th>NoOfEmployees</th>
                                                <th>Industry</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @isset($leads)
                                        @foreach($leads->success as $response)
                                            <tr>
                                                <td style="border-right:none;"><a href='EditLeads/{{ $response->id }}'><i class="fa fa-edit"></i></a>&nbsp;<a href='addevents/{{ $response->id }}'><i class="fa fa-plus"></i></a>&nbsp;<a onclick="deleteLead({{ $response->id }});" style="color:red; cursor:pointer;"><i class="fa fa-trash"></i></a> </td>
                                                <td>{{$response->LeadNo}}</td>
                                                <td style="display:none;">{{$response->LeadOwner}}</td>
                                                <td>{{$response->CompanyName}}</td>
                                                <td>{{$response->ContactPerson}}</td>
                                                <td>{{$response->ContactNumber}}</td>
                                                <td>{{$response->ContactEmailId}}</td>
                                                <td>{{$response->Address}}</td>
                                                <td>{{$response->City}}</td>
                                                <td>{{$response->State}}</td>
                                                <td>{{$response->PinCode}}</td>
                                                <td>{{$response->WebsiteURL}}</td>
                                                <td style="display:none;">{{$response->RecordCreatedBy}}</td>
                                                <td>{{$response->FirstName}} {{$response->LastName}}</td>
                                                <td>{{$response->EventOnline}}</td>
                                                <td>{{$response->CustomerTurnover}}</td>
                                                <td>{{$response->CustomerItSpend}}</td>
                                                <td>{{$response->SourceName}}</td>
                                                <td>{{$response->StatusTag}}</td>
                                                <td>{{$response->NoOfEmployees}}</td>
                                                <td>{{$response->IndustryName}}</td>
                                            </tr>
                                            <a href='deletelead/{{ $response->id }}' style="display:none;"  id="deletebutton{{$response->id}}">delete</a>
                                        @endforeach
                                        @endisset
                                        </tbody>        
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Virtuzo. Designed and Developed by <a href="https://virtuzo.in">Virtuzo</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    
    <script>
    function deleteLead(id)
    {
        debugger;
        swal({
        //title: "Are you sure you want to delete?",
         text: "Are you sure you want to delete?",
        // icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) 
        {
            document.getElementById("deletebutton"+id).click();
        } 
        else 
        {
            swal("Your Data is safe!");
        }
        });
    }
    </script>

    <script>
    $(document).ready(function() {
        // $("button").click(function(){
        var favorite = [];
        $.each($("input[type='checkbox']:checked"), function(){            
            favorite.push($(this).val());
            var id = this.value;
            var dt = $('table').DataTable();
            dt.columns(id).visible(!$(this).is(':checked'))
        });
           // alert("My favourite sports are: " + favorite.join(", "));
        // });
    });
    </script>

    <script>
        /****************************************
         *              Basic Table             *
         ****************************************/
        //$('#zero_config').DataTable();

        var myTable = $('#zero_config').DataTable({
        fixedColumns: true,
        "bSort": false,
        "columnDefs": [{ 
            'targets': [0],
            "orderable": false, 
            "bAutoWidth": false,
            "sClass": 'options'
        }]
    });
        // var trIndex = null;
        // $("#zero_config tr td").mouseenter(function () {
        //     trIndex = $(this).parent();
        //     $(trIndex).find("td:first-child").html('<a href="{{url("addevents")}}" style="color:grey"><i class="fa fa-plus"></i></a>&nbsp;<a href="{{url("form")}}" style="color:grey"><i class="fa fa-edit"></i></a>');
        // });

        // // remove button on tr mouseleave

        // $("#zero_config tr td").mouseleave(function () {
        //     $(trIndex).find('td:first-child').html("&nbsp;");
        // });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('input[type="checkbox"]').click(function(){
                var id = this.value;
                var dt = $('table').DataTable();
                dt.columns(id).visible(!$(this).is(':checked'))
            
            });
        });
    </script>
</body>

</html>