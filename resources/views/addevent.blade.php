@include('include.connect')
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>Matrix Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/extra-libs/multicheck/multicheck.css">
    <link href="/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link href="/dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
.dataTables_filter {
    display: none;
}
th, td {
    white-space: nowrap;
}

#example{
    border-bottom: none;
}
#example tr:first-child td:not(.options){
    border-bottom: 1px solid;
}
#example .options{
    background: white;
    border: none;
}
table.dataTable {
    clear: both;
    margin-top: 6px !important;
    margin-bottom: 6px !important;
    max-width: none !important;
    border-collapse: collapse !important;
}
</style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

    @include('include.sidebar')
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Leads Information</h4>
                        <div class="ml-auto text-right">
                            <!-- <button class="btn btn-primary btn-sm"  onclick="AddEvent();">Add Events</button> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <form action="{{url('SaveEvents')}}" method="POST">
            {{csrf_field()}}
            
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        @foreach($leaddetails->success as $masterdata)
                        <input type="hidden" name="hndbdId" value="{{$masterdata->Assigned_BD}}">
                        <div class="card">
                            <div class="card-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-5">
                                    <h5>Assigned To :</h5>
                                    <h5>Company Name :</h5>
                                    <h5>Contact Number :</h5>
                                    <h5>Website URL :</h5>
                                </div>
                                <div class="col-md-7 text-left">
                                <div style="font-size:17px;  font-family: arial;">{{$masterdata->FirstName}} {{$masterdata->LastName}}</div>
                                <div style="font-size:17px;  font-family: arial;">{{$masterdata->CompanyName}}</div>
                                <div style="font-size:17px;  font-family: arial;">{{$masterdata->ContactNumber}}</div>
                                <div style="font-size:17px;  font-family: arial;">{{$masterdata->WebsiteURL}}</div>
                                    </div>
                                    </div>
                            </div>
                            <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-5">
                                    <h5>Lead Number :</h5>
                                    <h5>Contact Person :</h5>
                                    <h5>Contact EmailID :</h5>
                                    <h5>Record Created By :</h5>
                                </div>
                                <div class="col-md-7 text-left">
                                <div style="font-size:17px; font-family: arial;">{{$masterdata->LeadNo}}</div>
                                <div style="font-size:17px; font-family: arial;">{{$masterdata->ContactPerson}}</div>
                                <div style="font-size:17px; font-family: arial;">{{$masterdata->ContactEmailId}}</div>
                                <div style="font-size:17px; font-family: arial;">{{$masterdata->RecordCreatedBy}}</div>
                                    </div>
                                    </div>
                            </div>
                                </div>
                            </div>
                        </div>                              
                        <input type="hidden" name="hdnfield" id="hdnfield" value="{{$masterdata->id}}">
                        
                        @endforeach
<!------------------------------- Event Details Start ----------------------------------->
                        <div class="card">
                            <div class="card-body">
                            <div class="row">
                            <div class="col-md-8" style="border-right: 1px solid rgba(0, 0, 0, 0.1)">
                            <h4>Event Information</h4>
                            <hr>
                                <div class="col-md-3" style="float:left;" id='datetimepicker1'>
                                
                                    <label>Event Date</label>
                                    <div class="input-group">
                                        <input type="text" name="EventDate" class="form-control datepicker-autoclose"  style="border-color:lightblue;" id="EventDate" placeholder="mm/dd/yyyy" value=""  required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3" style="float:left;">
                                    <label for="">Event Type</label>
                                    <select name="EventType" id="EventType" class="form-control" style="border-color:lightblue;"  required>
                                        <option value="">Choose Type</option>
                                        @foreach($eventtype->success as $response)
                                        <option value="{{$response->id}}">{{$response->EventName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3" style="float:left;">
                                    <label for="">Action Item</label>
                                    <input type="text" name="ActionItem" id="ActionItem" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                                <div class="col-md-3" style="float:left;">
                                    <label for="">Action DateTime</label>
                                    <div class="input-group">
                                        <input type="text" name="ActionDateTime" class="form-control datepicker-autoclose" id="ActionDateTime" placeholder="mm/dd/yyyy" style="border-color:lightblue;" value=""  required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3" style="float:left;">
                                <br>
                                    <label for="">Event Time Spend</label>
                                    <input type="text" name="EventTimeSpend" id="EventTimeSpend" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                                <div class="col-md-3" style="float:left;">
                                <br>
                                    <label for="">Communication</label>
                                    <select name="CommunicationMedium" id="CommunicationMedium" class="form-control" style="border-color:lightblue;"  required>
                                        <option value="">Choose Medium</option>
                                        @foreach($commediumlist->success as $response)
                                        <option value="{{$response->id}}">{{$response->CommunicationMedium}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6" style="top:5%; float:left;">
                                <textarea name="DiscussionSummery" id="DiscussionSummery" class="form-control" rows="3" placeholder="Event summuary...." style="border-color:lightblue; "  required></textarea>
                                </div>
                               
                            </div>
                            <hr>
                            <div class="col-md-4">
                            <h4 style="float:left;">Person Meet Details</h4>
                            <h5 style="float:right"><button Type="submit" id="Savebtn" class="btn btn-primary btn-sm">Save</button></h5>
                            <br>
                            <hr>
                                <div class="col-md-6" style="float:left;">
                                    <label for="">Name</label>
                                    <input type="text" name="PersonName" id="PersonName" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                                <div class="col-md-6" style="float:left;">
                                    <label for="">Contact No</label>
                                    <input type="text" name="PersonContactNo" id="PersonContactNo" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                                <div class="col-md-6" style="float:left;">
                                <br>
                                    <label for="">Email Id</label>
                                    <input type="email" name="PersonEmailId" id="PersonEmailId" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                                <div class="col-md-6" style="float:left;">
                                <br>
                                    <label for="">Designation</label>
                                    <input type="text" name="PersonDesignation" id="PersonDesignation" class="form-control" style="border-color:lightblue;" value=""  required>
                                </div>
                            </div>

                        </div>

                            </div>
                        </div>
                       
                       <div id="result">
                       
                       </div>
<!--------------------------------- Event Details End  ----------------------------->

<!------------------------------- Event Details Start ----------------------------------->
<!-- <div class="card">
                            <div class="card-body">
                            <div class="row">
                            <div class="col-md-8" style="border-right: 1px solid rgba(0, 0, 0, 0.1)">
                            <h4>Event Information</h4>
                            <hr>
                                <div class="col-md-4" style="float:left;" id='datetimepicker1'>
                                    <label for="">Event Date</label>
                                    <input type="text" name="EventDate" id="EventDate" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-4" style="float:left;">
                                    <label for="">Event Type</label>
                                    <select name="EventType" id="" class="form-control" style="border-color:lightblue;">
                                        <option value="">Choose Type</option>
                                        @foreach($eventtype->success as $response)
                                        <option value="{{$response->id}}">{{$response->EventName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4" style="float:left;">
                                    <label for="">Action Item</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-4" style="float:left;">
                                <br>
                                    <label for="">Action DateTime</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-4" style="float:left;">
                                <br>
                                    <label for="">Event Time Spend</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-4" style="float:left;">
                                <br>
                                    <label for="">Communication Medium</label>
                                    <select name="CommunicationMedium" id="CommunicationMedium" class="form-control" style="border-color:lightblue;">
                                        <option value="">Choose Medium</option>
                                        @foreach($commediumlist->success as $response)
                                        <option value="{{$response->id}}">{{$response->CommunicationMedium}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-12" style="top:5%; float:left;">
                                <textarea name="DiscussionSummery" id="DiscussionSummery" class="form-control" rows="4" placeholder="Event summuary...." style="border-color:lightblue; "></textarea>
                                </div>
                               
                            </div>
                            <hr>
                            <div class="col-md-4">
                            <h4>Person Meet Details</h4>
                            <hr>
                                <div class="col-md-6" style="float:left;">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-6" style="float:left;">
                                    <label for="">Contact No</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-6" style="float:left;">
                                <br>
                                    <label for="">Email Id</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                                <div class="col-md-6" style="float:left;">
                                <br>
                                    <label for="">Designation</label>
                                    <input type="text" class="form-control" style="border-color:lightblue;">
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="row">
                        <div class="col-md-10">

                        </div>
                        <div class="col-md-2 pull-right" style="padding-left:10%;">
                            <button class="btn btn-primary btn-sm pull-right">Save</button>
                        
                        </div>
                        </div>
                            </div>
                        </div> -->
<!--------------------------------- Event Details End  ----------------------------->

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
                </form>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            @include('include.footer')
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="/dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Sweetalert popup -->
    
    <script>
    @if(session('status'))
        swal('Event save successfully!');
    @endif
    </script>

    <script>
    function deleteitem()
    {
        //$("#result").InnerHTML.remove();
        $("#result").children().last().remove(); 
    }  
   
    function gethdnid(id)
    {
        debugger;
        $('#hdneventid').val(id);
        document.getElementById('formsubmit'+id).click();
    }
    </script>
    <script>
    jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });
    </script>

    
</body>

</html>