@include('include.connect')
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>Matrix Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" haref="/assets/libs/jquery-minicolors/jquery.minicolors.css">
    <link rel="stylesheet" type="text/css" href="/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/libs/quill/dist/quill.snow.css">
    <link href="/dist/css/style.min.css" rel="stylesheet">

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
       <div class="alert alert-success" role="alert">
           {{Session::get('status')}}
       </div>
    @endif

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        @include('include.sidebar')
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- ============== Start row ================ -->
                
                <form method="POST" action="{{url('SaveLeads')}}">
                {{csrf_field()}}
                <div class="row">
                    <!--============== Start col-md-6 ============== -->
                    <div class="col-md-6">
                    <h4>Personal Information</h4>
                        <div class="card">
                            <div class="card-body">
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>Company Name</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" name="CompanyName" class="form-control" placeholder="Company Name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>Contact person</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" name="ContactPerson" class="form-control" placeholder="Contact person" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>Contact Number</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" name="ContactNumber" class="form-control" placeholder="Contact Number" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>Contact EmailId</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" name="ContactEmailId" class="form-control" placeholder="Contact EmailId" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                    <!--============= End col-md-6 ===============-->

                    
                    <!--============== Start col-md-6 ============== -->
                    <div class="col-md-6">
                    <h4>Address Information</h4>
                        <div class="card">
                            <div class="card-body">
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>Address</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            
                                            <input type="text" name="Address" class="form-control" placeholder="Address" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>City</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            
                                            <input type="text" name="City" class="form-control" placeholder="City" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>State</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            
                                            <input type="text" name="State" class="form-control" placeholder="State" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3 align-items-center">
                                    <div class="col-lg-4 col-md-12 text-right">
                                        <span>PIN Code</span>
                                    </div>
                                    <div class="col-lg-8 col-md-12">
                                        <div class="input-group">
                                            
                                            <input type="text" name="PinCode" class="form-control" placeholder="PIN Code" maxlength="6" aria-label="Username" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                    <!--============= End col-md-6 ===============-->
                </div>
                <!-- ============= End Row ============= -->

                <div class="row"> <!--============= Start Row ===========-->
                    <div class="col-md-12"> <!--============= Start col-md-12 ===========-->
                        <div class="card"> <!--============= Start Card ===========-->
                            <!--============= Start Row ===========-->
                            <div class="row">
                                <!--============== Start col-md-6 ============== -->
                                <div class="col-md-6">
                                    <div class="card-body">
                                        <div class="row mb-3 align-items-center">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>Website URL</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-globe"></i></span>
                                                    </div>
                                                    <input type="text" name="WebsiteURL" class="form-control" placeholder="Website URL" aria-label="Username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 align-items-center" style="display:none;">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>Record Created By</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" name="RecordCreatedBy" class="form-control" value="0" placeholder="Record Created By" aria-label="Username">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 align-items-center">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>Assigned BD</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <select name="AssignedBD" id="AssignedBD" class="form-control" required>
                                                        <option value="">Choose User</option>
                                                        @foreach($userlist->success as $userdata)
                                                        <option value="{{$userdata->LoginID}}">{{$userdata->FirstName}} {{$userdata->LastName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 align-items-center" style="display:none;">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>Event Date</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="EventOnline" class="form-control" placeholder="Event Date" aria-label="Username" value="0">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 align-items-center">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>Lead Source</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-arrows-alt"></i></span>
                                                    </div>
                                                    <select name="LeadSource" id="LeadSource" class="form-control" required>
                                                        <option value="">Choose Status</option>
                                                        @foreach($sourcelist->success as $response)
                                                        <option value="{{$response->id}}">{{$response->SourceName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3 align-items-center">
                                            <div class="col-lg-4 col-md-12 text-right">
                                                <span>No Of Employee</span>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" name="NoOfEmployee" class="form-control" placeholder="No Of Employee" aria-label="Username" required>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    </div>
                                    <!--============= End col-md-6 ===============-->

                                    <!--============= Start col-md-6 ===============-->

                                        <div class="col-md-6">
                                            <div class="card-body">
                                                <!-- <div class="row mb-3 align-items-center">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Discussion Summary</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-list"></i></span>
                                                            </div>
                                                            <input type="text" name="DiscussionSummery" class="form-control" placeholder="Discussion Summary" aria-label="Username" >
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="row mb-3 align-items-center">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Customer Turnover</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar"></i></span>
                                                            </div>
                                                            <input type="text" name="CustomerTurnOver" class="form-control" placeholder="Customer Turnover" aria-label="Username" aria-describedby="basic-addon1" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 align-items-center">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Customer IT Spend</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar"></i></span>
                                                            </div>
                                                            <input type="text" name="CustomerItSpend" class="form-control" placeholder="Customer IT Spend" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 align-items-center" style="display:none;">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Customer IT Penetration</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                                            </div>
                                                            <input type="text" name="CustomerItPen" class="form-control" placeholder="Customer IT Penetration" value="0" aria-label="Username" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 align-items-center">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Lead Status</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                                            </div>
                                                            <select name="LeadStatus" id="LeadStatus" class="form-control" required>
                                                                <option value="">Choose Status</option>
                                                                @foreach($statuslist->success as $response)
                                                                <option value="{{$response->id}}">{{$response->StatusTag}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3 align-items-center">
                                                    <div class="col-lg-4 col-md-12 text-right">
                                                        <span>Industry</span>
                                                    </div>
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                                            </div>
                                                            <select name="Industry" id="Industry" class="form-control" required>
                                                                <option value="">Choose Status</option>
                                                                @foreach($industrylist->success as $response)
                                                                <option value="{{$response->id}}">{{$response->IndustryName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    <!--============= End col-md-6 ===============-->
                                    </div>
                                    <!--============= End row ===============-->
                                </div> <!--============= End Card ===========-->
                            <!--=============== div for button ============-->
                            <div class="row ">
                                <button type="submit" class="btn btn-primary btn-outline btn-md" style="width:135px; margin-left:88%;">Save Details</button>
                            </div>
                            <!--=============== End button ============-->
                        </div> <!--============= End col-md-12===========-->
                    </div> <!--============= End Row ===========-->
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                </form>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="/dist/js/pages/mask/mask.init.js"></script>
    <script src="/assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="/assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="/assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="/assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="/assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/libs/quill/dist/quill.min.js"></script>
    <script src="/https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
</body>

</html>