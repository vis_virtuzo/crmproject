<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;

class LeadStatusList
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = new Client();
        $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadStatusList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $data = $res->getBody();
        $GetStatusList = json_decode($data);
        return view('form',['statuslist'=>$GetStatusList]);
    }
}
