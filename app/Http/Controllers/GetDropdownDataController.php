<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class GetDropdownDataController extends Controller
{
    public function AllList()
    {
        $client = new Client();
        $resstatus = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadStatusList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datastatus = $resstatus->getBody();
        $GetStatusList = json_decode($datastatus);
        //return view('form',['statuslist'=>$GetStatusList]);

        $ressource = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadSourceList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datasource = $ressource->getBody();
        $GetSourceList = json_decode($datasource);

        $resindustry = $client->request('POST','http://18.222.158.193/CRMAPI/api/IndustryList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $dataindustry = $resindustry->getBody();
        $GetIndustryList = json_decode($dataindustry);

        $resusers = $client->request('POST','http://18.222.158.193/CRMAPI/api/UsersList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datausers = $resusers->getBody();
        $GetusersList = json_decode($datausers);
       
        return view('form',['statuslist'=>$GetStatusList,'sourcelist'=>$GetSourceList,'industrylist'=>$GetIndustryList,'userlist'=>$GetusersList]);
    }

    public function EventsAllList($id)
    {
        $client = new Client();

        // **************** Get Event Type Dropdown Data***************//

        $reseventtype = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadEventTypeList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $dataeventtype = $reseventtype->getBody();
        $GetEventType = json_decode($dataeventtype);

        // **************** Get Communication Medium Dropdown Data***************//

        $rescom = $client->request('POST','http://18.222.158.193/CRMAPI/api/ComMediumList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datacom = $rescom->getBody();
        $GetComMedium = json_decode($datacom);
        
        // **************** Get Master Data(Lead Details)***************//

        $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/getlead', array(
            'form_params' => array(
                'LeadId' => $id
                )
            ));
        $data = $res->getBody();
        $Leads = json_decode($data);
        
        // **************** Get Event Data Behalf of master data ***************//
        $resleadevents = $client->request('POST','http://18.222.158.193/CRMAPI/api/GetLeadEvents', array(
            'form_params' => array(
                'LeadId' => $id,
                'LeadEventId' => 0,
                )
            ));
        $dataevents = $resleadevents->getBody();
        $Leadevents = json_decode($dataevents);
         $resleadevents->getReasonPhrase();

         if($dataevents=='{"success":"No record found!"}')
         {
             return view('addevent',['eventtype'=>$GetEventType,'commediumlist'=>$GetComMedium,'leaddetails'=>$Leads]);
         }
         else
         {
            return view('addevents',['eventtype'=>$GetEventType,'commediumlist'=>$GetComMedium,'leaddetails'=>$Leads,'eventsdetail'=>$Leadevents]);
         }
    }
}
