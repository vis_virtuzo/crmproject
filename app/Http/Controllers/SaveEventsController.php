<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailtoAdminForEventUpdate;
use Session;
class SaveEventsController extends Controller
{
    public function store(Request $request)
    {   
        $SessionName = Session::get('Name');
        $request->all();
        $Assign_BD = $request->input('hndbdId');
        $LeadId = $request->input('hdnfield');
        $EventDate = $request->input('EventDate');
        $EventType = $request->input('EventType');
        $DiscussionSummery = $request->input('DiscussionSummery');
        $ActionItem = $request->input('ActionItem');
        $ActionDateTime = $request->input('ActionDateTime');
        $EventTimeSpend = $request->input('EventTimeSpend');
        $CommunicationMedium = $request->input('CommunicationMedium');
        $PersonName = $request->input('PersonName');
        $PersonContactNo = $request->input('PersonContactNo');
        $PersonEmailId = $request->input('PersonEmailId');
        $PersonDesignation = $request->input('PersonDesignation');

        $client = new Client();

        $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/AddEvent',array(
            'form_params' => array(
                'LeadEventId'=> 0,
                'LeadId' => $LeadId,
                'EventDate' => $EventDate,
                'EventType' => $EventType,
                'DiscussionSummery' => $DiscussionSummery,
                'ActionItem' => $ActionItem,
                'ActionDateTime' =>$ActionDateTime,
                'EventTimeSpend' => $EventTimeSpend,
                'ComMedium' => $CommunicationMedium,
                'PersonName' => $PersonName,
                'PersonContactNo' => $PersonContactNo,
                'PersonEmailId' => $PersonEmailId,
                'PersonDesignation' => $PersonDesignation,
                'Action' => 1
            )
        ));

        // ------------------*************** Call api for get Lead creator Id *************---------------//
        $GetRecordCreatedByDetails = $client->request('POST','http://18.222.158.193/CRMAPI/api/getlead', array(
            'form_params' => array(
                'LeadId' => $LeadId
                )
            ));
        $DataofRecordCreatedByDetails = $GetRecordCreatedByDetails->getBody();
        $RecordCreatedById = json_decode($DataofRecordCreatedByDetails)->success[0]->RecordCreatedBy;

        // ----------*********** Get lead creator name and email id behalf of creator id ***********-------------//
        $resusers = $client->request('POST','http://18.222.158.193/CRMAPI/api/UsersList',array(
            'form_params' => array(
                'id'=> $RecordCreatedById,
            )
            ));
            
        $datausers = $resusers->getBody();
        $RecordCreatorName = json_decode($datausers)->success[0]->FirstName;

        $RecordCreatorEmail = json_decode($datausers)->success[0]->EmailID;

        $data = array(
            'name' => $RecordCreatorName,
            'NameOfBD' => $SessionName
        );
        //-----------********** send mail to lead creator **********----------//
        Mail::to($RecordCreatorEmail)->send(new SendMailtoAdminForEventUpdate($data));

        return redirect('getleads')->with('status','Event save sucessfully');
    }

    public function update(Request $request)
    {
        $LeadId = $request->input('hdnfield');
        $LeadEventId = $request->input('hdneventid');
        $EventDate = $request->input('EventDate'.$LeadEventId);
        $EventType = $request->input('EventType'.$LeadEventId);
        $DiscussionSummery = $request->input('DiscussionSummery'.$LeadEventId);
        $ActionItem = $request->input('ActionItem'.$LeadEventId);
        $ActionDateTime = $request->input('ActionDateTime'.$LeadEventId);
        $EventTimeSpend = $request->input('EventTimeSpend'.$LeadEventId);
        $CommunicationMedium = $request->input('CommunicationMedium'.$LeadEventId);
        $PersonName = $request->input('PersonName'.$LeadEventId);
        $PersonContactNo = $request->input('PersonContactNo'.$LeadEventId);
        $PersonEmailId = $request->input('PersonEmailId'.$LeadEventId);
        $PersonDesignation = $request->input('PersonDesignation'.$LeadEventId);


        $client = new Client();
        $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/UpdateEvent',array(
            'form_params' => array(
                'LeadEventId'=> $LeadEventId,
                'LeadId' => $LeadId,
                'EventDate' => $EventDate,
                'EventType' => $EventType,
                'DiscussionSummery' => $DiscussionSummery,
                'ActionItem' => $ActionItem,
                'ActionDateTime' =>$ActionDateTime,
                'EventTimeSpend' => $EventTimeSpend,
                'ComMedium' => $CommunicationMedium,
                'PersonName' => $PersonName,
                'PersonContactNo' => $PersonContactNo,
                'PersonEmailId' => $PersonEmailId,
                'PersonDesignation' => $PersonDesignation,
                'Action' => 2
            )
        ));
        return redirect('getleads')->with('status','Event Update sucessfully');
    }
}
