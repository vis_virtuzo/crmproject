<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class DeleteLeadsController extends Controller
{
    public function delete($id)
    {
        $client = new Client();
        $resstatus = $client->request('POST','http://18.222.158.193/CRMAPI/api/dlead',array(
            'form_params' => array(
                'LeadId'=> $id,
            )
            ));
        $datastatus = $resstatus->getBody();
        $GetStatusList = json_decode($datastatus);
        // return redirect()->back()->with('successmessage', 'Lead Delete Successfully');
        return redirect()->back()->with('message', 'Lead Deleted Successfully');
    }
}
