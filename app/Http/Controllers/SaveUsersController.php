<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SaveUsersController extends Controller
{
    public function store(Request $request)
    {
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $conpassword = $request->input('conpassword');

        $client = new Client();
        $resuser = $client->request('POST','http://18.222.158.193/CRMAPI/api/userRegister',array(
            'form_params' => array(
                'username'=> $username,
                'email'=> $email,
                'password'=> $password,
                'conpassword' => $conpassword
            )
            ));
        $datauser = $resuser->getBody();
        $Getresponse = json_decode($datauser);
        return redirect('login');
    }
}
