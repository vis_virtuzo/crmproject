<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailtoAdmin;
use App\Mail\SendMailtoBD;
use Session;
class SaveLeadsController extends Controller
{
    public function store(Request $request)
    {
        $RecordCreatedBy = Session::get('UserId');
        //return $request->all();

        $hdnId = $request->input('hdnfield');
        $CompanyName = $request->input('CompanyName');
        $ContactPerson = $request->input('ContactPerson');
        $ContactNumber = $request->input('ContactNumber');
        $ContactEmailId = $request->input('ContactEmailId');
        $Address = $request->input('Address');
        $City = $request->input('City');
        $State = $request->input('State');
        $PinCode = $request->input('PinCode');
        $WebsiteURL = $request->input('WebsiteURL');
        $RecordCreatedBy = $RecordCreatedBy;
        $AssignedBD = $request->input('AssignedBD');
        $EventOnline = $request->input('EventOnline');
        $LeadSource = $request->input('LeadSource');
        $NoOfEmployee = $request->input('NoOfEmployee');
        $CustomerTurnOver = $request->input('CustomerTurnOver');
        $CustomerItSpend = $request->input('CustomerItSpend');
        $CustomerItPen = $request->input('CustomerItPen');
        
        $LeadStatus = $request->input('LeadStatus');
        $Industry = $request->input('Industry');

        $client = new Client();

        //============= Call api for update existing lead =============//
        if($hdnId > 0)
        {
            $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/ulead',array(
                'form_params' => array(
                    'LeadId'=> $hdnId,
                    'LeadNo' => 'vcs222',
                    'LeadOwner' => 1,
                    'CompanyName' => $CompanyName,
                    'ContactPerson' => $ContactPerson,
                    'ContactNumber' => $ContactNumber,
                    'ContactEmailId' =>$ContactEmailId,
                    'Address' => $Address,
                    'City' => $City,
                    'State' => $State,
                    'PinCode' => $PinCode,
                    'WebsiteURL' => $WebsiteURL,
                    'RecordCreatedBy' => $RecordCreatedBy,
                    'Assigned_BD' => $AssignedBD,
                    'EventOnline' => $EventOnline,
                    'CustomerTurnover' => $CustomerTurnOver,
                    'CustomerItSpend' => $CustomerItSpend,
                    'CustomerItPen' => $CustomerItPen,
                    'LeadSource' => $LeadSource,
                    'LeadStatus' => $LeadStatus,
                    'NoOfEmployees' => $NoOfEmployee,
                    'Industry' => $Industry,
                    'CreatedBy' => 1,
                    'isActive' => 1
                )
            ));
        }
        else
        {
            //============= Call api for save new lead =============//
            $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/slead',array(
                'form_params' => array(
                    'LeadId'=> 0,
                    'LeadNo' => 'vcs222',
                    'LeadOwner' => 1,
                    'CompanyName' => $CompanyName,
                    'ContactPerson' => $ContactPerson,
                    'ContactNumber' => $ContactNumber,
                    'ContactEmailId' =>$ContactEmailId,
                    'Address' => $Address,
                    'City' => $City,
                    'State' => $State,
                    'PinCode' => $PinCode,
                    'WebsiteURL' => $WebsiteURL,
                    'RecordCreatedBy' => $RecordCreatedBy,
                    'Assigned_BD' => $AssignedBD,
                    'EventOnline' => $EventOnline,
                    'CustomerTurnover' => $CustomerTurnOver,
                    'CustomerItSpend' => $CustomerItSpend,
                    'CustomerItPen' => $CustomerItPen,
                    'LeadSource' => $LeadSource,
                    'LeadStatus' => $LeadStatus,
                    'NoOfEmployees' => $NoOfEmployee,
                    'Industry' => $Industry,
                    'CreatedBy' => 1,
                    'isActive' => 1
                )
            ));
        }

        $data = $res->getBody();

        // -------------********** Call api for getting Assigned_BD id **********-------------//
        $resusers = $client->request('POST','http://18.222.158.193/CRMAPI/api/UsersList',array(
            'form_params' => array(
                'id'=> $AssignedBD,
            )
            ));
        $datausers = $resusers->getBody();
        $BD_Name = json_decode($datausers)->success[0]->FirstName;
        $BD_Email = json_decode($datausers)->success[0]->EmailID;
        
        $SessionEmail = Session::get('EmailId');
        $SessionName = Session::get('Name');
        //$response = json_decode($data);
        $data = array(
            'name' => $SessionName, 
            'BD_name' =>$AssignedBD
        );
        $data2 = array(
            'name' => $BD_Name 
        );
        Mail::to($SessionEmail)->send(new SendMailtoAdmin($data));

        Mail::to($BD_Email)->send(new SendMailtoBD($data2));
        
        return redirect('getleads')->with('status','Event Update sucessfully');
    }
}
?>