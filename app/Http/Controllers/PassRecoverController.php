<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class PassRecoverController extends Controller
{
    public function recover(Request $request)
    {
        $client = new Client();
        $getemail = $request->input('recoveremail');
        $resuser = $client->request('POST','http://18.222.158.193/CRMAPI/api/RecoverPassword',array(
            'form_params' => array(
            'email'=> $getemail
            )
            ));
        $datauser = $resuser->getBody();

        $status = json_decode($datauser)->success;
        if($status != 'Not found')
        {
            $FirstName = json_decode($datauser)->success[0]->FirstName;
            $LastName = json_decode($datauser)->success[0]->LastName;
            $CurrentPass = json_decode($datauser)->success[0]->Pwd;
            $EmailAddress = json_decode($datauser)->success[0]->EmailID;
    
            $FullName = $FirstName.' '.$LastName;
            
            $data = array(
                'name' => $FullName,
                'password' =>$CurrentPass
            );
            Mail::to($EmailAddress)->send(new SendMail($data));
            return back()->with('success', 'Password Sent To Your Email Address');
        }
        else
        {
            return back()->with('status', 'Invalid Email Address !');
        }

    }
}
