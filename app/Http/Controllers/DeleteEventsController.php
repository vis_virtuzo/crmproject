<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
class DeleteEventsController extends Controller
{
    public function delete($id)
    {
        $client = new Client();
        $resstatus = $client->request('POST','http://18.222.158.193/CRMAPI/api/DeleteEvent',array(
            'form_params' => array(
                'LeadEventId'=> $id,
            )
            ));
        $datastatus = $resstatus->getBody();
        $GetStatusList = json_decode($datastatus);
        return redirect()->back()->with('success', 'Event Delete Successfully'); 
        
    }
}
