<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class GetLeadsController extends Controller
{
    public function GetLead()
    {
        $client = new Client();

        $RoleId = Session::get('RoleId');
        $UserId = Session::get('UserId');
        if($RoleId == 1)
        {
            $res = $client->request('GET','http://18.222.158.193/CRMAPI/api/getleads');
            $data = $res->getBody();
            $leads = json_decode($data);
            return view('tables',['leads'=>$leads]);
        }
        else
        {
            $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/getleadsbehalfofrole',array(
            'form_params' => array(
                'UserId'=> $UserId,
            )
            ));
            $data = $res->getBody();
            $leads = json_decode($data);
            return view('tables',['leads'=>$leads]);
        }
        
    }

    public function GetSingleLead($id)
    {
        $client = new Client();
        $resstatus = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadStatusList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datastatus = $resstatus->getBody();
        $GetStatusList = json_decode($datastatus);
        //return view('form',['statuslist'=>$GetStatusList]);

        $ressource = $client->request('POST','http://18.222.158.193/CRMAPI/api/LeadSourceList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datasource = $ressource->getBody();
        $GetSourceList = json_decode($datasource);

        $resindustry = $client->request('POST','http://18.222.158.193/CRMAPI/api/IndustryList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $dataindustry = $resindustry->getBody();
        $GetIndustryList = json_decode($dataindustry);
       
        
        $res = $client->request('POST','http://18.222.158.193/CRMAPI/api/getlead', array(
            'form_params' => array(
                'LeadId' => $id
                )
            ));
        $data = $res->getBody();
        $Leads = json_decode($data);

        $resusers = $client->request('POST','http://18.222.158.193/CRMAPI/api/UsersList',array(
            'form_params' => array(
                'id'=> 0,
            )
            ));
        $datausers = $resusers->getBody();
        $GetusersList = json_decode($datausers);

        //return view('form',['lead'=>$Leads]);
        return view('editlead',['statuslist'=>$GetStatusList,'sourcelist'=>$GetSourceList,'industrylist'=>$GetIndustryList,'lead'=>$Leads,'userlist'=>$GetusersList]);


    }

}
