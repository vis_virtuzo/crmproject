<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store;
use GuzzleHttp\Client;
use Session;

class LoginChkController extends Controller
{
    public function Login(Request $request)
    {
        $username = $request->input('email');
        $password = $request->input('password');
        
        $client = new Client();
        $resuser = $client->request('POST','http://18.222.158.193/CRMAPI/api/userLogin',array(
            'form_params' => array(
                'email'=> $username,
                'password'=> $password,
                'isActive'=> 1
            )
            ));
        $datauser = $resuser->getBody();
        //$Getresponse = json_decode($datauser);
        if($datauser == '{"success":"Not found"}')
        {
            return redirect()->back()->with('status', 'Wrong Credentials !');
        }
        else
        {
            $userID = json_decode($datauser)->success[0]->LoginID;
            $RoleId = json_decode($datauser)->success[0]->Role;
            $emailid = json_decode($datauser)->success[0]->EmailID;
            $name = json_decode($datauser)->success[0]->FirstName;
            session(['UserId' => $userID]);
            session(['RoleId' => $RoleId]);
            session(['EmailId' => $emailid]);
            session(['Name' => $name]);
            Session::get('UserId');
            Session::get('RoleId');
            Session::get('EmailId');
            return view('index');
        }
    }
}
