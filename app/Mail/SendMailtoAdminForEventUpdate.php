<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailtoAdminForEventUpdate extends Mailable
{
    use Queueable, SerializesModels;
    public $maildetails;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($maildetails)
    {
        $this->maildetails = $maildetails;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('jayarya352@gmail.com')->subject('New Event Add')->view('MailtoAdminForEvent')->with('data',$this->maildetails);
    }
}
